<?php
/**
 * @file
 * ethical_documents.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function ethical_documents_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'add terms in oe_document_type'.
  $permissions['add terms in oe_document_type'] = array(
    'name' => 'add terms in oe_document_type',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
    ),
    'module' => 'taxonomy_access_fix',
  );

  // Exported permission: 'delete terms in oe_document_type'.
  $permissions['delete terms in oe_document_type'] = array(
    'name' => 'delete terms in oe_document_type',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'edit terms in oe_document_type'.
  $permissions['edit terms in oe_document_type'] = array(
    'name' => 'edit terms in oe_document_type',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
    ),
    'module' => 'taxonomy',
  );

  return $permissions;
}
