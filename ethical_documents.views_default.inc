<?php
/**
 * @file
 * ethical_documents.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function ethical_documents_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'openethical_documents';
  $view->description = '';
  $view->tag = 'openethical';
  $view->base_table = 'file_managed';
  $view->human_name = 'Open Ethical Documents';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['distinct'] = TRUE;
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '3';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'list';
  $handler->display->display_options['style_options']['default_row_class'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['row_options']['hide_empty'] = TRUE;
  $handler->display->display_options['row_options']['default_field_elements'] = FALSE;
  /* Relationship: Field: Country (field_country) */
  $handler->display->display_options['relationships']['field_country_iso2']['id'] = 'field_country_iso2';
  $handler->display->display_options['relationships']['field_country_iso2']['table'] = 'field_data_field_country';
  $handler->display->display_options['relationships']['field_country_iso2']['field'] = 'field_country_iso2';
  /* Field: Field: Primary Image */
  $handler->display->display_options['fields']['field_featured_image']['id'] = 'field_featured_image';
  $handler->display->display_options['fields']['field_featured_image']['table'] = 'field_data_field_featured_image';
  $handler->display->display_options['fields']['field_featured_image']['field'] = 'field_featured_image';
  $handler->display->display_options['fields']['field_featured_image']['label'] = '';
  $handler->display->display_options['fields']['field_featured_image']['element_type'] = '0';
  $handler->display->display_options['fields']['field_featured_image']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_featured_image']['element_wrapper_type'] = '0';
  $handler->display->display_options['fields']['field_featured_image']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_featured_image']['hide_empty'] = TRUE;
  $handler->display->display_options['fields']['field_featured_image']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_featured_image']['settings'] = array(
    'image_style' => 'portrait',
    'image_link' => '',
  );
  /* Field: File: Name */
  $handler->display->display_options['fields']['filename']['id'] = 'filename';
  $handler->display->display_options['fields']['filename']['table'] = 'file_managed';
  $handler->display->display_options['fields']['filename']['field'] = 'filename';
  $handler->display->display_options['fields']['filename']['label'] = '';
  $handler->display->display_options['fields']['filename']['element_type'] = '0';
  $handler->display->display_options['fields']['filename']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['filename']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['filename']['link_to_file'] = FALSE;
  /* Field: File: Author */
  $handler->display->display_options['fields']['field_file_author']['id'] = 'field_file_author';
  $handler->display->display_options['fields']['field_file_author']['table'] = 'field_data_field_file_author';
  $handler->display->display_options['fields']['field_file_author']['field'] = 'field_file_author';
  $handler->display->display_options['fields']['field_file_author']['label'] = '';
  $handler->display->display_options['fields']['field_file_author']['element_type'] = 'p';
  $handler->display->display_options['fields']['field_file_author']['element_class'] = 'author';
  $handler->display->display_options['fields']['field_file_author']['element_label_type'] = '0';
  $handler->display->display_options['fields']['field_file_author']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_file_author']['element_wrapper_type'] = '0';
  $handler->display->display_options['fields']['field_file_author']['element_default_classes'] = FALSE;
  /* Field: File: Date published */
  $handler->display->display_options['fields']['field_file_published_date']['id'] = 'field_file_published_date';
  $handler->display->display_options['fields']['field_file_published_date']['table'] = 'field_data_field_file_published_date';
  $handler->display->display_options['fields']['field_file_published_date']['field'] = 'field_file_published_date';
  $handler->display->display_options['fields']['field_file_published_date']['label'] = '';
  $handler->display->display_options['fields']['field_file_published_date']['element_type'] = 'p';
  $handler->display->display_options['fields']['field_file_published_date']['element_class'] = 'date';
  $handler->display->display_options['fields']['field_file_published_date']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_file_published_date']['element_wrapper_type'] = '0';
  $handler->display->display_options['fields']['field_file_published_date']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_file_published_date']['hide_empty'] = TRUE;
  $handler->display->display_options['fields']['field_file_published_date']['settings'] = array(
    'format_type' => 'panopoly_day',
    'fromto' => 'both',
    'multiple_number' => '',
    'multiple_from' => '',
    'multiple_to' => '',
  );
  /* Field: File: Description */
  $handler->display->display_options['fields']['field_file_description']['id'] = 'field_file_description';
  $handler->display->display_options['fields']['field_file_description']['table'] = 'field_data_field_file_description';
  $handler->display->display_options['fields']['field_file_description']['field'] = 'field_file_description';
  $handler->display->display_options['fields']['field_file_description']['label'] = '';
  $handler->display->display_options['fields']['field_file_description']['element_type'] = 'div';
  $handler->display->display_options['fields']['field_file_description']['element_class'] = 'description';
  $handler->display->display_options['fields']['field_file_description']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_file_description']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_file_description']['hide_empty'] = TRUE;
  /* Field: File: Rendered */
  $handler->display->display_options['fields']['rendered']['id'] = 'rendered';
  $handler->display->display_options['fields']['rendered']['table'] = 'file_managed';
  $handler->display->display_options['fields']['rendered']['field'] = 'rendered';
  $handler->display->display_options['fields']['rendered']['label'] = '';
  $handler->display->display_options['fields']['rendered']['element_type'] = '0';
  $handler->display->display_options['fields']['rendered']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['rendered']['element_wrapper_type'] = '0';
  $handler->display->display_options['fields']['rendered']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['rendered']['file_view_mode'] = 'link';
  /* Sort criterion: File: Date published (field_file_published_date) */
  $handler->display->display_options['sorts']['field_file_published_date_value']['id'] = 'field_file_published_date_value';
  $handler->display->display_options['sorts']['field_file_published_date_value']['table'] = 'field_data_field_file_published_date';
  $handler->display->display_options['sorts']['field_file_published_date_value']['field'] = 'field_file_published_date_value';
  $handler->display->display_options['sorts']['field_file_published_date_value']['exposed'] = TRUE;
  $handler->display->display_options['sorts']['field_file_published_date_value']['expose']['label'] = 'Date published (field_file_published_date)';
  /* Sort criterion: File: Name */
  $handler->display->display_options['sorts']['filename']['id'] = 'filename';
  $handler->display->display_options['sorts']['filename']['table'] = 'file_managed';
  $handler->display->display_options['sorts']['filename']['field'] = 'filename';
  /* Sort criterion: File: Upload date */
  $handler->display->display_options['sorts']['timestamp']['id'] = 'timestamp';
  $handler->display->display_options['sorts']['timestamp']['table'] = 'file_managed';
  $handler->display->display_options['sorts']['timestamp']['field'] = 'timestamp';
  /* Sort criterion: Global: Random */
  $handler->display->display_options['sorts']['random']['id'] = 'random';
  $handler->display->display_options['sorts']['random']['table'] = 'views';
  $handler->display->display_options['sorts']['random']['field'] = 'random';
  /* Filter criterion: File: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'file_managed';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'document' => 'document',
  );
  $handler->display->display_options['filters']['type']['group'] = 1;
  /* Filter criterion: File: Document type (field_oe_document_type) */
  $handler->display->display_options['filters']['field_oe_document_type_tid']['id'] = 'field_oe_document_type_tid';
  $handler->display->display_options['filters']['field_oe_document_type_tid']['table'] = 'field_data_field_oe_document_type';
  $handler->display->display_options['filters']['field_oe_document_type_tid']['field'] = 'field_oe_document_type_tid';
  $handler->display->display_options['filters']['field_oe_document_type_tid']['operator'] = 'and';
  $handler->display->display_options['filters']['field_oe_document_type_tid']['group'] = 1;
  $handler->display->display_options['filters']['field_oe_document_type_tid']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_oe_document_type_tid']['expose']['operator_id'] = 'field_oe_document_type_tid_op';
  $handler->display->display_options['filters']['field_oe_document_type_tid']['expose']['label'] = 'Type';
  $handler->display->display_options['filters']['field_oe_document_type_tid']['expose']['operator'] = 'field_oe_document_type_tid_op';
  $handler->display->display_options['filters']['field_oe_document_type_tid']['expose']['identifier'] = 'field_oe_document_type_tid';
  $handler->display->display_options['filters']['field_oe_document_type_tid']['expose']['multiple'] = TRUE;
  $handler->display->display_options['filters']['field_oe_document_type_tid']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    4 => 0,
  );
  $handler->display->display_options['filters']['field_oe_document_type_tid']['type'] = 'select';
  $handler->display->display_options['filters']['field_oe_document_type_tid']['vocabulary'] = 'oe_document_type';
  /* Filter criterion: Field: Project (field_project) */
  $handler->display->display_options['filters']['field_project_target_id']['id'] = 'field_project_target_id';
  $handler->display->display_options['filters']['field_project_target_id']['table'] = 'field_data_field_project';
  $handler->display->display_options['filters']['field_project_target_id']['field'] = 'field_project_target_id';
  $handler->display->display_options['filters']['field_project_target_id']['group'] = 1;
  $handler->display->display_options['filters']['field_project_target_id']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_project_target_id']['expose']['operator_id'] = 'field_project_target_id_op';
  $handler->display->display_options['filters']['field_project_target_id']['expose']['label'] = 'Project';
  $handler->display->display_options['filters']['field_project_target_id']['expose']['operator'] = 'field_project_target_id_op';
  $handler->display->display_options['filters']['field_project_target_id']['expose']['identifier'] = 'field_project_target_id';
  $handler->display->display_options['filters']['field_project_target_id']['expose']['multiple'] = TRUE;
  $handler->display->display_options['filters']['field_project_target_id']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    4 => 0,
  );
  /* Filter criterion: Field: Category (field_featured_categories) */
  $handler->display->display_options['filters']['field_featured_categories_tid']['id'] = 'field_featured_categories_tid';
  $handler->display->display_options['filters']['field_featured_categories_tid']['table'] = 'field_data_field_featured_categories';
  $handler->display->display_options['filters']['field_featured_categories_tid']['field'] = 'field_featured_categories_tid';
  $handler->display->display_options['filters']['field_featured_categories_tid']['operator'] = 'and';
  $handler->display->display_options['filters']['field_featured_categories_tid']['value'] = '';
  $handler->display->display_options['filters']['field_featured_categories_tid']['group'] = 1;
  $handler->display->display_options['filters']['field_featured_categories_tid']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_featured_categories_tid']['expose']['operator_id'] = 'field_featured_categories_tid_op';
  $handler->display->display_options['filters']['field_featured_categories_tid']['expose']['label'] = 'Category';
  $handler->display->display_options['filters']['field_featured_categories_tid']['expose']['operator'] = 'field_featured_categories_tid_op';
  $handler->display->display_options['filters']['field_featured_categories_tid']['expose']['identifier'] = 'field_featured_categories_tid';
  $handler->display->display_options['filters']['field_featured_categories_tid']['expose']['multiple'] = TRUE;
  $handler->display->display_options['filters']['field_featured_categories_tid']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    4 => 0,
  );
  $handler->display->display_options['filters']['field_featured_categories_tid']['type'] = 'select';
  $handler->display->display_options['filters']['field_featured_categories_tid']['vocabulary'] = 'panopoly_categories';
  /* Filter criterion: Countries: Name - list */
  $handler->display->display_options['filters']['name_list']['id'] = 'name_list';
  $handler->display->display_options['filters']['name_list']['table'] = 'countries_country';
  $handler->display->display_options['filters']['name_list']['field'] = 'name_list';
  $handler->display->display_options['filters']['name_list']['relationship'] = 'field_country_iso2';
  $handler->display->display_options['filters']['name_list']['group'] = 1;
  $handler->display->display_options['filters']['name_list']['exposed'] = TRUE;
  $handler->display->display_options['filters']['name_list']['expose']['operator_id'] = 'name_list_op';
  $handler->display->display_options['filters']['name_list']['expose']['label'] = 'Country';
  $handler->display->display_options['filters']['name_list']['expose']['operator'] = 'name_list_op';
  $handler->display->display_options['filters']['name_list']['expose']['identifier'] = 'name_list';
  $handler->display->display_options['filters']['name_list']['expose']['multiple'] = TRUE;
  $handler->display->display_options['filters']['name_list']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    4 => 0,
  );
  $handler->display->display_options['filters']['name_list']['configuration'] = '1';
  $handler->display->display_options['filters']['name_list']['filter'] = array(
    'enabled' => '1',
    'continents' => array(),
  );
  /* Filter criterion: Field: Group (field_group) */
  $handler->display->display_options['filters']['field_group_tid']['id'] = 'field_group_tid';
  $handler->display->display_options['filters']['field_group_tid']['table'] = 'field_data_field_group';
  $handler->display->display_options['filters']['field_group_tid']['field'] = 'field_group_tid';
  $handler->display->display_options['filters']['field_group_tid']['operator'] = 'and';
  $handler->display->display_options['filters']['field_group_tid']['value'] = '';
  $handler->display->display_options['filters']['field_group_tid']['group'] = 1;
  $handler->display->display_options['filters']['field_group_tid']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_group_tid']['expose']['operator_id'] = 'field_group_tid_op';
  $handler->display->display_options['filters']['field_group_tid']['expose']['label'] = 'Group';
  $handler->display->display_options['filters']['field_group_tid']['expose']['operator'] = 'field_group_tid_op';
  $handler->display->display_options['filters']['field_group_tid']['expose']['identifier'] = 'field_group_tid';
  $handler->display->display_options['filters']['field_group_tid']['expose']['multiple'] = TRUE;
  $handler->display->display_options['filters']['field_group_tid']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    4 => 0,
  );
  $handler->display->display_options['filters']['field_group_tid']['type'] = 'select';
  $handler->display->display_options['filters']['field_group_tid']['vocabulary'] = 'tag';

  /* Display: List of documents */
  $handler = $view->new_display('panel_pane', 'List of documents', 'list_of_documents');
  $handler->display->display_options['defaults']['sorts'] = FALSE;
  /* Sort criterion: File: Date published (field_file_published_date) */
  $handler->display->display_options['sorts']['field_file_published_date_value']['id'] = 'field_file_published_date_value';
  $handler->display->display_options['sorts']['field_file_published_date_value']['table'] = 'field_data_field_file_published_date';
  $handler->display->display_options['sorts']['field_file_published_date_value']['field'] = 'field_file_published_date_value';
  $handler->display->display_options['sorts']['field_file_published_date_value']['order'] = 'DESC';
  $handler->display->display_options['sorts']['field_file_published_date_value']['exposed'] = TRUE;
  $handler->display->display_options['sorts']['field_file_published_date_value']['expose']['label'] = 'Date published';
  /* Sort criterion: File: Upload date */
  $handler->display->display_options['sorts']['timestamp']['id'] = 'timestamp';
  $handler->display->display_options['sorts']['timestamp']['table'] = 'file_managed';
  $handler->display->display_options['sorts']['timestamp']['field'] = 'timestamp';
  $handler->display->display_options['sorts']['timestamp']['order'] = 'DESC';
  $handler->display->display_options['sorts']['timestamp']['exposed'] = TRUE;
  $handler->display->display_options['sorts']['timestamp']['expose']['label'] = 'Upload date';
  /* Sort criterion: File: Name */
  $handler->display->display_options['sorts']['filename']['id'] = 'filename';
  $handler->display->display_options['sorts']['filename']['table'] = 'file_managed';
  $handler->display->display_options['sorts']['filename']['field'] = 'filename';
  $handler->display->display_options['sorts']['filename']['exposed'] = TRUE;
  $handler->display->display_options['sorts']['filename']['expose']['label'] = 'Title';
  /* Sort criterion: Global: Random */
  $handler->display->display_options['sorts']['random']['id'] = 'random';
  $handler->display->display_options['sorts']['random']['table'] = 'views';
  $handler->display->display_options['sorts']['random']['field'] = 'random';
  $handler->display->display_options['sorts']['random']['exposed'] = TRUE;
  $handler->display->display_options['sorts']['random']['expose']['label'] = 'Random';
  $handler->display->display_options['pane_title'] = 'List of documents';
  $handler->display->display_options['pane_category']['name'] = 'General';
  $handler->display->display_options['pane_category']['weight'] = '0';
  $handler->display->display_options['allow']['use_pager'] = 0;
  $handler->display->display_options['allow']['items_per_page'] = 'items_per_page';
  $handler->display->display_options['allow']['offset'] = 0;
  $handler->display->display_options['allow']['link_to_view'] = 0;
  $handler->display->display_options['allow']['more_link'] = 0;
  $handler->display->display_options['allow']['path_override'] = 0;
  $handler->display->display_options['allow']['title_override'] = 'title_override';
  $handler->display->display_options['allow']['exposed_form'] = 'exposed_form';
  $handler->display->display_options['allow']['fields_override'] = 'fields_override';

  /* Display: Document */
  $handler = $view->new_display('panel_pane', 'Document', 'panel_pane_1');
  $handler->display->display_options['defaults']['pager'] = FALSE;
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '1';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['style_options']['row_class'] = 'clearfix';
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['row_options']['hide_empty'] = TRUE;
  $handler->display->display_options['row_options']['default_field_elements'] = FALSE;
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: File: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'file_managed';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'document' => 'document',
  );
  $handler->display->display_options['filters']['type']['group'] = 1;
  /* Filter criterion: File: Name */
  $handler->display->display_options['filters']['filename']['id'] = 'filename';
  $handler->display->display_options['filters']['filename']['table'] = 'file_managed';
  $handler->display->display_options['filters']['filename']['field'] = 'filename';
  $handler->display->display_options['filters']['filename']['operator'] = 'contains';
  $handler->display->display_options['filters']['filename']['exposed'] = TRUE;
  $handler->display->display_options['filters']['filename']['expose']['operator_id'] = 'filename_op';
  $handler->display->display_options['filters']['filename']['expose']['label'] = 'File name';
  $handler->display->display_options['filters']['filename']['expose']['operator'] = 'filename_op';
  $handler->display->display_options['filters']['filename']['expose']['identifier'] = 'filename';
  $handler->display->display_options['filters']['filename']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    4 => 0,
  );
  $handler->display->display_options['filters']['filename']['expose']['autocomplete_filter'] = 1;
  $handler->display->display_options['filters']['filename']['expose']['autocomplete_items'] = '10';
  $handler->display->display_options['filters']['filename']['expose']['autocomplete_field'] = 'filename';
  $handler->display->display_options['filters']['filename']['expose']['autocomplete_raw_suggestion'] = 1;
  $handler->display->display_options['filters']['filename']['expose']['autocomplete_raw_dropdown'] = 1;
  $handler->display->display_options['filters']['filename']['expose']['autocomplete_dependent'] = 0;
  /* Filter criterion: File: File ID */
  $handler->display->display_options['filters']['fid']['id'] = 'fid';
  $handler->display->display_options['filters']['fid']['table'] = 'file_managed';
  $handler->display->display_options['filters']['fid']['field'] = 'fid';
  $handler->display->display_options['filters']['fid']['exposed'] = TRUE;
  $handler->display->display_options['filters']['fid']['expose']['operator_id'] = 'fid_op';
  $handler->display->display_options['filters']['fid']['expose']['label'] = 'Document (FID)';
  $handler->display->display_options['filters']['fid']['expose']['operator'] = 'fid_op';
  $handler->display->display_options['filters']['fid']['expose']['identifier'] = 'fid';
  $handler->display->display_options['filters']['fid']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    4 => 0,
  );
  $handler->display->display_options['pane_title'] = 'Document';
  $handler->display->display_options['pane_category']['name'] = 'General';
  $handler->display->display_options['pane_category']['weight'] = '0';
  $handler->display->display_options['allow']['use_pager'] = 0;
  $handler->display->display_options['allow']['items_per_page'] = 0;
  $handler->display->display_options['allow']['offset'] = 0;
  $handler->display->display_options['allow']['link_to_view'] = 0;
  $handler->display->display_options['allow']['more_link'] = 0;
  $handler->display->display_options['allow']['path_override'] = 0;
  $handler->display->display_options['allow']['title_override'] = 0;
  $handler->display->display_options['allow']['exposed_form'] = 'exposed_form';
  $handler->display->display_options['allow']['fields_override'] = 'fields_override';
  $export['openethical_documents'] = $view;

  return $export;
}
