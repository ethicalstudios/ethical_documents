<?php
/**
 * @file
 * ethical_documents.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function ethical_documents_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "ds" && $api == "ds") {
    return array("version" => "1");
  }
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "file_entity" && $api == "file_default_displays") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function ethical_documents_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_image_default_styles().
 */
function ethical_documents_image_default_styles() {
  $styles = array();

  // Exported image style: portrait.
  $styles['portrait'] = array(
    'effects' => array(
      5 => array(
        'name' => 'manualcrop_crop_and_scale',
        'data' => array(
          'width' => 240,
          'height' => 339,
          'upscale' => 0,
          'respectminimum' => 1,
          'onlyscaleifcrop' => 0,
          'style_name' => 'portrait',
        ),
        'weight' => 0,
      ),
    ),
    'label' => 'portrait',
  );

  return $styles;
}
